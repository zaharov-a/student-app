<?php

use Ox3a\Common\StudentApplication;
use Zend\Http\PhpEnvironment\Request;

/** @var StudentApplication $app */
$app = require_once __DIR__ . '/../app.php';
$app->bootstrap();
/** @var Request $request */
$request = $app->getRequest();
$file    = __DIR__ . $request->getRequestUri();
// Отдача статических файлов
if (is_file($file)) {
    $type = mime_content_type($file);
    if ($type == 'text/x-asm') {
        $type = 'text/css';
    } else {
        if ('js' == substr($file, -2)) {
            $type = 'application/javascript';
        } else {
            if ('css' == substr($file, -3)) {
                $type = 'text/css';
            }
        }
    }
    header("Content-type: {$type}");
    readfile($file);
} else {
    $app->run();
}
