<?php

use Ox3a\Common\StudentApplication;
use Ox3a\ExampleModule\ExampleModule;

require __DIR__ . '/../vendor/autoload.php';

$app = StudentApplication::init(
    [
        'configDirs' => [
            __DIR__ . '/configs',
        ],
        'modules'    => [
            new ExampleModule(),
        ],
    ]
);

return $app;
