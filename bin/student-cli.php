#!/usr/bin/env php
<?php

$composerInstall = '';
foreach (
    [
        __DIR__ . '/../../../autoload.php',
        __DIR__ . '/../vendor/autoload.php',
        __DIR__ . '/../autoload.php',
    ] as $file
) {
    if (file_exists($file)) {
        $composerInstall = $file;

        break;
    }
}

unset($file);

require $composerInstall;

use Ox3a\Common\Command\StaticCollectCommand;
use Ox3a\Common\Command\RouteGenerateCommand;
use Symfony\Component\Console\Application;

$cli = new Application();

$cli->add(new RouteGenerateCommand());
$cli->add(new StaticCollectCommand());

if (defined('GET_CLI_APPLICATION')) {
    return $cli;
} else {
    try {
        $cli->run();
    } catch (Exception $e) {
        echo $e->getMessage();
        print_r($e->getTrace());
    }
}
