<?php


namespace Ox3a\ExampleModule\Controller;


use Ox3a\Annotation\Acl;
use Ox3a\Annotation\IsGranted;
use Ox3a\Annotation\Route;
use Ox3a\Common\Controller\AbstractController;
use Ox3a\Common\View\HtmlView;

class IndexController extends AbstractController
{
    /**
     * @var HtmlView
     */
    protected $_htmlView;


    /**
     * IndexController constructor.
     * @param HtmlView $_htmlView
     */
    public function __construct(HtmlView $_htmlView)
    {
        $this->_htmlView = $_htmlView;
    }


    /**
     * @Route("/error", name="error")
     * @IsGranted(IsGranted::IS_AUTHENTICATED_FULLY)
     */
    public function indexAction()
    {
        throw new \RuntimeException('ss', 1400);
        // return $this->_htmlView;
    }


    /**
     * @Route("/test/acl", name="acl")
     * @Acl("test>access", expected=1)
     */
    public function aclAction()
    {
    }
}
