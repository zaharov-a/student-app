<?php
return array (
  'error' => 
  array (
    'type' => 'segment',
    'options' => 
    array (
      'route' => '/error',
      'constraints' => 
      array (
      ),
      'defaults' => 
      array (
        '_action' => 'indexAction',
        '_actionName' => 'index',
        '_controller' => 'Ox3a\\ExampleModule\\Controller\\IndexController',
        '_controllerDir' => 'index',
        '_moduleName' => 'ExampleModule',
      ),
    ),
  ),
  'acl' => 
  array (
    'type' => 'segment',
    'options' => 
    array (
      'route' => '/test/acl',
      'constraints' => 
      array (
      ),
      'defaults' => 
      array (
        '_action' => 'aclAction',
        '_actionName' => 'acl',
        '_controller' => 'Ox3a\\ExampleModule\\Controller\\IndexController',
        '_controllerDir' => 'index',
        '_moduleName' => 'ExampleModule',
      ),
    ),
  ),
);