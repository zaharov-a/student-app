<?php


namespace Ox3a\ExampleModule;


use Ox3a\Common\Module\ModuleInterface;
use Ox3a\Common\StudentApplication;

class ExampleModule implements ModuleInterface
{
    protected $_path = __DIR__;

    /**
     * @var StudentApplication
     */
    protected $_app;


    /**
     * @param StudentApplication $app
     * @return ExampleModule
     */
    public function setApp(StudentApplication $app)
    {
        $this->_app = $app;
        return $this;
    }


    public function getName()
    {
        $className = static::class;
        return substr($className, strrpos($className, '\\') + 1);
    }


    public function getConfigDir()
    {
        return $this->_path . '/configs';
    }


    public function getResourceDir()
    {
        return $this->_path . '/Resources';
    }


    public function bootstrap()
    {
    }
}
