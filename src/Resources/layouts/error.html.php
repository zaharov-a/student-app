<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Ошибка</title>
    <style>
        body {
            background-color: black;
            color: lightgray;
        }

        .container {
            width: 1000px;
            margin: 0 auto;
        }

        .error {
            display: flex;
            font-size: 150%;
        }

        pre {
            color: darkgreen;
            background: #111;
            overflow: auto;
            padding: 20px;
        }

    </style>
</head>
<body>
<div class="container">
    <h1>Ошибка</h1>
    <div class="error">
        <div><?php echo $this->code ?: '' ?></div>
        <div><?php echo $this->message ?></div>
    </div>
    <?php if ($this->errorData) { ?>
        <pre><?php echo str_replace(
                ['\/', '\\\\'],
                ['/', '\\'],
                json_encode($this->errorData, JSON_PRETTY_PRINT)
            ) ?></pre>
    <?php } ?>
</div>
</body>
</html>
