<?php

// пример слоя
$this->css()
    ->addFile(
        'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css',
        [
            'integrity'   => "sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u",
            'crossorigin' => "anonymous",
        ]
    )
    ->addFile(
        'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css',
        [
            'integrity'   => "sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp",
            'crossorigin' => "anonymous",
        ]
    );

$this->css()->addFile('/static/example-module/css/example.css');

$this->js()
    ->addFile('https://code.jquery.com/jquery-3.4.1.min.js')
    ->addFile(
        'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js',
        [
            'integrity'   => "sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa",
            'crossorigin' => "anonymous",
        ]
    );
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <title></title>
    <?php echo $this->css() ?>
</head>
<body>
<?php
echo $this->getContent();
?>
<?php echo $this->js() ?>
</body>
</html>
