<?php

namespace Ox3a\Common\View;

use Ox3a\Common\View\HtmlView\HelperContainer;
use Ox3a\Service\ConfigServiceInterface;
use Psr\Container\ContainerInterface;

/**
 * Created by PhpStorm.
 * User: aa
 * Date: 22.03.19
 * Time: 21:41
 */
class HtmlView implements ViewInterface
{
    protected $_contentType = 'text/html';

    protected $_statusCode = 200;

    protected $_data = [];

    protected $_layout = 'default';

    protected $_layoutsDir = 'Resources/layouts';

    protected $_templatesDir;

    protected $_template;

    /**
     * @var string
     */
    protected $_content;

    /**
     * @var ConfigServiceInterface
     */
    protected $_configService;


    /**
     * Список хелперов
     * @var ContainerInterface
     */
    protected $_helpers = [];

    /**
     * @var bool
     */
    protected $_contentIsEnable = true;


    /**
     * HtmlView constructor.
     * @param ConfigServiceInterface $_configService
     * @param HelperContainer        $_helpers
     */
    public function __construct(ConfigServiceInterface $_configService, HelperContainer $_helpers)
    {
        $config               = $_configService->app['resources']['layout'];
        $this->_configService = $_configService;
        $this->_helpers       = $_helpers;

        $this->_layoutsDir = $config['dir'];
        $this->_layout     = $config['template'];
    }


    public function __get($name)
    {
        return array_key_exists($name, $this->_data) ? $this->_data[$name] : null;
    }


    public function __call($name, $arguments)
    {
        $helper = $this->_helpers->get($name);
        $helper->setView($this);

        return $arguments ? call_user_func_array($helper, $arguments) : $helper;
        // if (substr($name, 0, 3) === 'get') {
        //     $name = substr($name, 3);
        //
        //     if (isset($this->_helpers[$name])) {
        //         $helper = $this->_helpers[$name];
        //         return $arguments ? call_user_func_array($helper, $arguments) : $helper;
        //     } else {
        //         throw new InvalidArgumentException('Неизвестный вызов get' . $name);
        //     }
        // }
    }


    /**
     * @return ContainerInterface
     */
    public function getHelpers()
    {
        return $this->_helpers;
    }




    // /**
    //  * Установить хелпер
    //  * @param string          $name
    //  * @param HelperInterface $helper
    //  * @return $this
    //  */
    // public function setHelper($name, $helper)
    // {
    //     $helper->setView($this);
    //     $this->_helpers[$name] = $helper;
    //     return $this;
    // }


    /**
     * @return string
     */
    public function getContentType()
    {
        return $this->_contentType;
    }


    public function toString()
    {
        $layoutName = $this->_layoutsDir . '/' . $this->_layout . '.html.php';

        if (!is_file($layoutName)) {
            return sprintf('Layout "%s" not found', $layoutName);
        }

        if ($this->contentIsEnable()) {
            if (!$this->_template) {
                return 'Template is empty';
            }

            $fullName = $this->getTemplateFullName();

            if (!is_file($fullName)) {
                return sprintf('Template "%s" not found', $fullName);
            }

            $this->_content = $this->render();
        }

        include $layoutName;

        return ob_get_clean();
    }


    public function setResult($data)
    {
        $this->_data = $data;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getTemplate()
    {
        return $this->_template;
    }


    /**
     * @param mixed $template
     * @return HtmlView
     */
    public function setTemplate($template)
    {
        $this->_template = $template;
        return $this;
    }


    /**
     * @return string
     */
    public function getLayout()
    {
        return $this->_layout;
    }


    /**
     * @param string $layout
     * @return HtmlView
     */
    public function setLayout($layout)
    {
        $this->_layout = $layout;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getTemplatesDir()
    {
        return $this->_templatesDir;
    }


    /**
     * @param mixed $templatesDir
     * @return HtmlView
     */
    public function setTemplatesDir($templatesDir)
    {
        $this->_templatesDir = $templatesDir;
        return $this;
    }


    /**
     * @return string
     */
    public function getLayoutsDir()
    {
        return $this->_layoutsDir;
    }


    /**
     * @param string $layoutsDir
     * @return HtmlView
     */
    public function setLayoutsDir($layoutsDir)
    {
        $this->_layoutsDir = $layoutsDir;
        return $this;
    }


    public function includeTemplate($template = null)
    {
        require $this->getTemplateFullName($template);
    }


    public function getTemplateFullName($template = null)
    {
        if (!$template) {
            $template = $this->_template;
        }
        return $this->_templatesDir . '/' . $template . '.html.php';
    }


    public function getContent()
    {
        return $this->_content;
    }


    public function render()
    {
        ob_start();

        $this->includeTemplate();

        $content = ob_get_clean();

        ob_start();

        return $content;
    }


    public function disableContent()
    {
        $this->_contentIsEnable = false;
    }


    public function contentIsEnable()
    {
        return $this->_contentIsEnable;
    }


    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->_statusCode;
    }


    /**
     * @param int $statusCode
     * @return $this
     */
    public function setStatusCode($statusCode)
    {
        $this->_statusCode = $statusCode;
        return $this;
    }
}
