<?php
/**
 * Created by PhpStorm.
 * User: aa
 * Date: 22.03.19
 * Time: 22:38
 */

namespace Ox3a\Common\View;

// https://habr.com/ru/post/441854/
class JsonRpc20View implements ViewInterface
{

    protected $_options = 0;

    protected $_contentType = 'application/json';

    protected $_statusCode = 200;

    protected $_error;

    protected $_result;

    protected $_id;


    /**
     * @return string
     */
    public function getContentType()
    {
        return $this->_contentType;
    }


    public function toString()
    {
        return json_encode($this->getResponse(), $this->_options);
    }


    public function getResponse()
    {
        $response = [
            'jsonrpc' => '2.0',
            'id'      => $this->_id,
        ];

        if ($this->_error) {
            $response['error'] = $this->_error;
        } else {
            $response['result'] = $this->_result;
        }

        return $response;
    }


    /**
     * @param int    $code
     * @param string $message
     * @param mixed  $data
     * @return $this
     */
    public function setError($code, $message, $data = null)
    {
        $this->_error = [
            'code'    => (int)$code,
            'message' => (string)$message,
        ];

        if ($data) {
            $this->_error['data'] = $data;
        }

        return $this;
    }


    /**
     * @param mixed $result
     * @return JsonRpc20View
     */
    public function setResult($result)
    {
        $this->_result = $result;
        return $this;
    }


    /**
     * @param mixed $id
     * @return JsonRpc20View
     */
    public function setId($id)
    {
        $this->_id = $id;
        return $this;
    }


    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->_statusCode;
    }


    /**
     * @param int $statusCode
     * @return $this
     */
    public function setStatusCode($statusCode)
    {
        $this->_statusCode = $statusCode;
        return $this;
    }


}
