<?php


namespace Ox3a\Common\View;


class RawView implements ViewInterface
{

    protected $_contentType = null;

    protected $_statusCode = 200;

    protected $_result = null;


    /**
     * RawView constructor.
     * @param string $_contentType
     * @param string $_result
     */
    public function __construct($_contentType, $_result)
    {
        $this->_contentType = $_contentType;
        $this->_result      = $_result;
    }


    public function getContentType()
    {
        return $this->_contentType;
    }


    public function toString()
    {
        return $this->_result;
    }


    /**
     * @param string $data
     * @return $this
     */
    public function setResult($data)
    {
        $this->_result = $data;
        return $this;
    }


    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->_statusCode;
    }


    /**
     * @param int $statusCode
     * @return $this
     */
    public function setStatusCode($statusCode)
    {
        $this->_statusCode = $statusCode;
        return $this;
    }


}
