<?php


namespace Ox3a\Common\View\HtmlView;


use Ox3a\Common\View\HtmlView;

class CssHelper implements HelperInterface
{

    protected $_list = [];

    /**
     * @var HtmlView
     */
    protected $_view;


    public function addFile($file, $params = [])
    {
        $this->_list[] = [
                'href' => $file,
            ] + $params;

        return $this;
    }


    public function setList($list)
    {
        $this->_list = $list;
        return $this;
    }


    public function setView($view)
    {
        $this->_view = $view;
        return $this;
    }


    public function render()
    {
        $view   = $this->_view;
        $result = [];

        foreach ($this->_list as $css) {
            $params = array_filter($css);
            $attr   = [];
            foreach ($params as $key => $value) {
                if ($key === 'href') {
                    $value = $view->url($value);
                }
                $attr[] = sprintf('%s="%s"', $key, $value);
            }
            $result[] = sprintf('<link rel="stylesheet" type="text/css" %s/>', implode(' ', $attr));
        }
        return implode("\n", $result);
    }


    public function __toString()
    {
        return $this->render();
    }


    public function __invoke()
    {
        return $this;
    }


}
