<?php


namespace Ox3a\Common\View\HtmlView;


use Ox3a\Common\Service\RouterService;

class RouteHelper implements HelperInterface
{

    /**
     * @var RouterService
     */
    protected $_routerService;

    protected $_routeName;

    protected $_params = [];


    /**
     * RouteHelper constructor.
     * @param RouterService $_routerService
     */
    public function __construct(RouterService $_routerService)
    {
        $this->_routerService = $_routerService;
    }


    public function setView($view)
    {
        // TODO: Implement setView() method.
    }


    public function render()
    {
        return $this->_routerService->getUrl($this->_routeName, $this->_params);
    }


    public function __toString()
    {
        return $this->render();
    }


    public function __invoke()
    {
        $argv = func_get_args();
        $this->setRouteName($argv[0]);
        if (isset($argv[1])) {
            $this->setParams($argv[1]);
        }
        return $this;
    }


    /**
     * @param mixed $routeName
     * @return RouteHelper
     */
    public function setRouteName($routeName)
    {
        $this->_routeName = $routeName;
        return $this;
    }


    /**
     * @param array $params
     * @return RouteHelper
     */
    public function setParams($params)
    {
        $this->_params = $params;
        return $this;
    }


}
