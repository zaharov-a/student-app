<?php


namespace Ox3a\Common\View\HtmlView;

use Ox3a\Common\View\HtmlView;

interface HelperInterface
{

    const IS_SHARE = true;


    /**
     * @param HtmlView $view
     * @return HelperInterface
     */
    public function setView($view);


    /**
     * @return string
     */
    public function render();


    // /**
    //  * @param array $params
    //  * @return HelperInterface
    //  */
    // public function setParams($params);


    /**
     * @return string
     */
    public function __toString();


    /**
     * @return HelperInterface|string
     */
    public function __invoke();
}
