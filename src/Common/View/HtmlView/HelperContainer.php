<?php


namespace Ox3a\Common\View\HtmlView;


use Ox3a\Service\ConfigServiceInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use RuntimeException;

class HelperContainer implements ContainerInterface
{

    protected $_helpers = [];

    protected $_definitions = [];

    /**
     * @var ContainerInterface
     */
    protected $_container;


    /**
     * HelperContainer constructor.
     * @param ContainerInterface     $container
     * @param ConfigServiceInterface $configService
     */
    public function __construct(ContainerInterface $container, ConfigServiceInterface $configService)
    {
        $this->_container = $container;

        $helpers = $configService->get('helpers');
        if (isset($helpers['view'])) {
            $this->_definitions = $helpers['view'];
        }
    }


    public function add($alias, $params)
    {
        unset($this->_helpers[$alias]);
        unset($this->_definitions[$alias]);
        $this->_definitions[$alias] = $params;

        return $this;
    }


    public function get($id)
    {
        if (!$this->has($id)) {
            throw new RuntimeException("Helper {$id} not found");
        }

        if (!isset($this->_helpers[$id])) {
            $this->_helpers[$id] = $this->_container->get($this->_definitions[$id]);
        }

        return $this->_helpers[$id];
    }


    public function has($id)
    {
        return isset($this->_definitions[$id]);
    }

}
