<?php


namespace Ox3a\Common\View\HtmlView;


use Zend\Http\PhpEnvironment\Request;
use Zend\Stdlib\RequestInterface;

class UrlHelper implements HelperInterface
{

    /**
     * @var Request
     */
    protected $_request;

    protected $_url;

    protected $_params = [];


    /**
     * UrlHelper constructor.
     * @param RequestInterface $_request
     */
    public function __construct(RequestInterface $_request)
    {
        $this->_request = $_request;
    }


    public function setView($view)
    {
        // TODO: Implement setView() method.
    }


    public function render()
    {
        $info = parse_url($this->_url);

        if (isset($info['host'])) {
            return $this->_url;
        }

        $query = http_build_query($this->_params);

        return $this->_request->getBaseUrl() . $this->_url . ($query ? ('?' . $query) : '');
    }


    public function __toString()
    {
        return $this->render();
    }


    public function __invoke()
    {
        $argv = func_get_args();
        $this->setUrl($argv[0]);
        $this->setParams(isset($argv[1]) ? $argv[1] : []);
        return $this;
    }


    /**
     * @param mixed $url
     * @return UrlHelper
     */
    public function setUrl($url)
    {
        $this->_url = $url;
        return $this;
    }


    /**
     * @param array $params
     * @return UrlHelper
     */
    public function setParams($params)
    {
        $this->_params = $params;
        return $this;
    }


}
