<?php


namespace Ox3a\Common\View\HtmlView;


use Ox3a\Common\View\HtmlView;

class JsHelper implements HelperInterface
{
    protected $_list = [];

    /**
     * @var HtmlView
     */
    protected $_view;


    public function addFile($file, $params = [])
    {
        $this->_list[] = [
                'src' => $file,
            ] + $params;
        return $this;
    }


    public function setList($list)
    {
        $this->_list = $list;
        return $this;
    }


    /**
     * @param HtmlView $view
     * @return $this|HelperInterface
     */
    public function setView($view)
    {
        $this->_view = $view;
        return $this;
    }


    public function render()
    {
        $result = [];

        foreach ($this->_list as $script) {
            $params = array_filter($script);
            $attr   = [];
            foreach ($params as $key => $value) {
                if ($key == 'src') {
                    $value = $this->_view->url($value);
                }
                $attr[] = sprintf('%s="%s"', $key, $value);
            }
            $result[] = sprintf('<script %s></script>', implode(' ', $attr));
        }
        return implode("\n", $result);
    }


    public function __toString()
    {
        return $this->render();
    }


    public function __invoke()
    {
        return $this;
    }

}
