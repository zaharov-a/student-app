<?php
/**
 * Created by PhpStorm.
 * User: aa
 * Date: 22.03.19
 * Time: 22:38
 */

namespace Ox3a\Common\View;


interface ViewInterface
{

    /**
     * @return int
     */
    public function getStatusCode();

    /**
     * @return string
     */
    public function getContentType();


    /**
     * @return string
     */
    public function toString();


    /**
     * @param mixed $data
     * @return mixed
     */
    public function setResult($data);

}
