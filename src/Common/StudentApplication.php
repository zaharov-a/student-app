<?php

namespace Ox3a\Common;

use Exception;
use League\Container\Container;
use League\Container\Definition\ClassDefinition;
use Ox3a\Common\Service\ErrorCatcherService;
use Ox3a\Common\Service\ReflectionContainerService;
use Ox3a\Common\Module\ModuleInterface;
use Ox3a\Service\ConfigService;
use Ox3a\Service\ConfigServiceInterface;
use Psr\Container\ContainerInterface;
use RuntimeException;
use Zend\Http\Response;
use Zend\Stdlib\DispatchableInterface;
use Zend\Stdlib\RequestInterface;

class StudentApplication
{
    /**
     * @var ContainerInterface
     */
    protected $_container;

    /**
     * @var mixed[]
     */
    protected $_params;

    /**
     * @var ModuleInterface[]
     */
    protected $_modules = [];

    /**
     * @var RequestInterface
     */
    protected $_request;


    /**
     * @var DispatchableInterface
     */
    protected $_dispatcher;


    /**
     * @param $params
     * @return static
     */
    public static function init($params)
    {
        $container = new Container();
        $container->delegate(new ReflectionContainerService());
        //
        $container->share(ContainerInterface::class, $container);
        $container->share('container', $container);

        return new static($container, $params);
    }


    /**
     * StudentApplication constructor.
     * @param ContainerInterface $container
     * @param mixed[]            $params
     */
    public function __construct(ContainerInterface $container, $params)
    {
        $this->_container = $container;

        $this->_params = $params;

        $this->_initModules();
    }


    /**
     *
     */
    public function bootstrap()
    {
        $container = $this->_container;

        $container->share('app', $this);
        $container->share(static::class, $this);

        $this->_bootstrapConfig();

        $this->_bootstrapServices();

        $this->_bootstrapModules();
    }


    /**
     * Выполнить
     */
    public function run()
    {
        try {
            /** @var Response $response */
            $response = $this->getDispatcher()->dispatch($this->getRequest(), new Response());

            header($response->renderStatusLine());

            foreach ($response->getHeaders() as $header) {
                header($header->toString());
            }

            echo $response->getBody();
        } catch (Exception $e) {
            $data = $this->getErrorCatcherService()->caught($e);
            echo $e->getCode();
            echo $e->getMessage();
            if ($data) {
                print_r($data);
            }
        }
    }


    /**
     * @return ContainerInterface
     */
    public function getContainer()
    {
        return $this->_container;
    }


    /**
     * @return DispatchableInterface
     */
    public function getDispatcher()
    {
        if (!$this->_dispatcher) {
            $this->_dispatcher = $this->_container->get(DispatchableInterface::class);
        }

        return $this->_dispatcher;
    }


    /**
     * @return RequestInterface
     */
    public function getRequest()
    {
        if (!$this->_request) {
            $request = $this->_container->get(RequestInterface::class);
            $config  = $this->_container->get(ConfigServiceInterface::class);

            if (isset($config->get('app')['request']['base_url'])) {
                $request->setBaseUrl($config->get('app')['request']['base_url']);
            }

            $this->_request = $request;
        }

        return $this->_request;
    }


    /**
     * Добавить модуль
     * @param ModuleInterface $module
     */
    public function addModule(ModuleInterface $module)
    {
        $module->setApp($this);
        $this->_modules[$module->getName()] = $module;
    }


    /**
     * @param $name
     * @return ModuleInterface
     */
    public function getModule($name)
    {
        if (!isset($this->_modules[$name])) {
            throw new RuntimeException(sprintf('Module "%s" not found', $name));
        }

        return $this->_modules[$name];
    }


    /**
     * @return ModuleInterface[]
     */
    public function getModules()
    {
        return $this->_modules;
    }


    /**
     * @return ErrorCatcherService
     */
    public function getErrorCatcherService()
    {
        return $this->getContainer()->get(ErrorCatcherService::class);
    }


    protected function _initModules()
    {
        $params = $this->_params;

        if (isset($params['modules'])) {
            /** @var ModuleInterface $module */
            foreach ($params['modules'] as $module) {
                $this->addModule($module);
            }
        }
    }


    protected function _bootstrapConfig()
    {
        $params    = $this->_params;
        $container = $this->_container;

        /** @var ConfigService $config */
        $container->share(ConfigService::class);
        $config = $container->get(ConfigService::class);

        $container->share('config', $config);
        $container->share(ConfigServiceInterface::class, $config);

        foreach ($this->getModules() as $module) {
            $config->addDir($module->getConfigDir());
        }

        // главный конфиг должен перезаписывать остальные
        if (isset($params['configDirs'])) {
            foreach ($params['configDirs'] as $configDir) {
                $config->addDir($configDir);
            }
        }
    }


    protected function _bootstrapServices()
    {
        /** @var ConfigService $config */
        $config       = $this->_container->get(ConfigService::class);
        $container    = $this->_container;
        $containerCfg = $config->container;

        if (isset($containerCfg['services'])) {
            foreach ($containerCfg['services'] as $alias => $service) {
                $concrete = isset($service['concrete']) ? $service['concrete'] : null;
                /** @var ClassDefinition $definition */
                $definition = $container->share($alias, $concrete);

                if (isset($service['arguments'])) {
                    foreach ($service['arguments'] as $argument) {
                        $definition->withArgument($argument);
                    }
                }
            }
        }
    }


    protected function _bootstrapModules()
    {
        foreach ($this->getModules() as $module) {
            $module->bootstrap();
        }
    }
}
