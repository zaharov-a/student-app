<?php


namespace Ox3a\Common\Command;

use Doctrine\Common\Annotations\AnnotationException;
use Ox3a\Service\CaseService;
use ReflectionException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Ox3a\Annotation\Analyzer;

class RouteGenerateCommand extends Command
{

    /**
     * @var string
     */
    protected $_root;


    protected function configure()
    {
        $this->setName('route:generate')// the name of the command (the part after "bin/console")
        ->setDescription(
            'Сгенерировать конфиги маршрутов и доступов к ним'
        )// the short description shown while running "php bin/console list"
        ;

        $this->addArgument('root', InputArgument::REQUIRED, 'Путь до проекта');
    }


    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws ReflectionException
     * @throws AnnotationException
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->_root    = realpath($input->getArgument('root'));
        $controllersDir = $this->_root . '/src/*Module/Controller/*';

        $controllerList = $this->getControllerList($controllersDir);
        $analyzer       = new Analyzer();
        $caseService    = new CaseService();

        foreach ($controllerList as $moduleDir => $controllers) {
            $routes    = [];
            $isGranted = [];
            $acl       = [];
            foreach ($controllers as $controller) {
                $annotations = $analyzer->run($controller['class']);

                foreach ($annotations as $actionName => $action) {
                    $module = basename($moduleDir);
                    $part   = explode(
                        '\\',
                        str_replace(
                            'Controller',
                            '',
                            preg_replace(
                                "/(.+){$module}\\\\Controller\\\\(.+)$/",
                                '$2',
                                $controller['class']
                            )
                        )
                    );

                    $extraData = [
                        '_action'        => $actionName,
                        '_actionName'    => $caseService->toKebabCase(
                            str_replace('Action', '', $actionName)
                        ),
                        '_controller'    => $controller['class'],
                        '_controllerDir' => implode(
                            '/',
                            array_map(
                                [
                                    $caseService,
                                    'toKebabCase',
                                ],
                                $part
                            )
                        ),
                        '_moduleName'    => $module,
                    ];
                    if (isset($action['Route'])) {
                        foreach ($action['Route'] as $route) {
                            $routes[$route['name']] = $route['params'];

                            foreach ($extraData as $key => $value) {
                                $routes[$route['name']]['options']['defaults'][$key] = $value;
                            }
                        }
                    }
                    if (isset($action['IsGranted'])) {
                        foreach ($action['IsGranted'] as $data) {
                            if (!isset($isGranted[$extraData['_controller']])) {
                                $isGranted[$extraData['_controller']] = [];
                            }
                            if (!isset($isGranted[$extraData['_controller']][$extraData['_action']])) {
                                $isGranted[$extraData['_controller']][$extraData['_action']] = [];
                            }
                            $isGranted[$extraData['_controller']][$extraData['_action']][] = $data;
                        }
                    }
                    if (isset($action['Acl'])) {
                        foreach ($action['Acl'] as $data) {
                            if (!isset($acl[$extraData['_controller']])) {
                                $acl[$extraData['_controller']] = [];
                            }
                            if (!isset($acl[$extraData['_controller']][$extraData['_action']])) {
                                $acl[$extraData['_controller']][$extraData['_action']] = [];
                            }
                            $acl[$extraData['_controller']][$extraData['_action']][] = $data;
                        }
                    }
                }
            }

            $this->saveFile($moduleDir . '/configs/routes.cfg.php', $routes);
            $this->saveFile($moduleDir . '/configs/granted.cfg.php', $isGranted);
            $this->saveFile($moduleDir . '/configs/acl.cfg.php', $acl);
        }

        $output->writeln('Ok');
    }


    public function getControllerList($dir)
    {
        $glob = glob($dir);
        $list = [];

        foreach ($glob as $file) {
            if (is_dir($file)) {
                $list = array_merge_recursive($list, $this->getControllerList($file . '/*'));
            } else {
                $info = $this->getControllerInfo($file);

                if (!isset($list[$info['dir']])) {
                    $list[$info['dir']] = [];
                }

                $list[$info['dir']][] = $info;
            }
        }

        return $list;
    }


    public function getControllerInfo($file)
    {
        require_once $file;
        $fileName = str_replace([$this->_root . '/src/', '.php'], '', $file);

        $parts = explode('/', $fileName);

        // $caseService     = CaseService::getInstance();
        // $controllerParts = array_map(function($part) use ($caseService) {
        //     return $caseService->toKebabCase(str_replace('Controller', '', $part));
        // }, array_slice($parts, 2));
        $classList = array_reverse(get_declared_classes());
        $className = '';
        $lastPart  = end($parts);

        foreach ($classList as $class) {
            if (preg_match("/{$lastPart}$/", $class)) {
                $className = $class;
                break;
            }
        }

        if (!$className) {
            print_r([$classList, $file]);
            throw new \RuntimeException('Обнаружился пустой класс');
        }
        // print_r(array($file, get_declared_classes()), false);
        return [
            'dir'   => $this->_root . "/src/{$parts[0]}",
            'file'  => $file,
            // 'module'     => $caseService->toKebabCase(str_replace('Module', '', $parts[0])),
            // 'controller' => implode('_', $controllerParts),
            'class' => $className,
        ];
    }


    private function saveFile($name, $data)
    {
        if ($data) {
            file_put_contents($name, "<?php\nreturn " . var_export($data, true) . ';');
        } else {
            if (is_file($name)) {
                unlink($name);
            }
        }
    }
}

