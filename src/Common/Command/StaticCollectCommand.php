<?php


namespace Ox3a\Common\Command;


use Doctrine\Common\Annotations\AnnotationException;
use Ox3a\Common\StudentApplication;
use Ox3a\Service\CaseService;
use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class StaticCollectCommand extends Command
{


    protected function configure()
    {
        $this->setName('static:collect')// the name of the command (the part after "bin/console")
        ->setDescription(
            'Собрать статические ресурсы модулей'
        )// the short description shown while running "php bin/console list"
        ;


        $this->addArgument('app-file', InputArgument::REQUIRED, 'Путь до приложения return $app');
        $this->addArgument('static-dir', InputArgument::REQUIRED, 'Папка со статическими ресурсами (будет очищена)');
        $this->addOption(
            'symlink',
            null,
            InputOption::VALUE_OPTIONAL,
            'Создавать ссылки на папки, вместо копирования',
            'y'
        );
    }


    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int|void|null
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $appFile = realpath($input->getArgument('app-file'));
        /** @var StudentApplication $app */
        $app       = require_once $appFile;
        $staticDir = realpath($input->getArgument('static-dir'));

        $symlink = $input->getOption('symlink');
        if (!in_array($symlink, ['y', 'n'])) {
            throw new RuntimeException('Неверное значение для флага symlink [y, n]');
        }

        $symlink = 'y' === $symlink;

        if (!is_dir($staticDir)) {
            throw new RuntimeException('Неверный путь');
        }

        $this->clearDir($staticDir);
        $caseService = new CaseService();

        foreach ($app->getModules() as $module) {
            $moduleStaticDir = $module->getResourceDir() . '/static';
            if (is_dir($moduleStaticDir)) {
                $moduleName = $caseService->toKebabCase($module->getName());
                if ($symlink) {
                    $this->createSymlink($moduleStaticDir, $staticDir . '/' . $moduleName);
                } else {
                    $this->copyDir($moduleStaticDir, $staticDir . '/' . $moduleName);
                }
            }
        }
    }


    public function copyDir($source, $target)
    {
        $dir = opendir($source);
        if (!is_dir($target)) {
            mkdir($target);
        }
        while (false !== ($file = readdir($dir))) {
            if (($file != '.') && ($file != '..')) {
                $s = $source . '/' . $file;
                $t = $target . '/' . $file;

                if (is_dir($s)) {
                    $this->copyDir($s, $t);
                } else {
                    copy($s, $t);
                }
            }
        }
        closedir($dir);
    }


    public function createSymlink($source, $target)
    {
        if (!symlink($source, $target)) {
            throw new RuntimeException('Ошибка создания символьной ссылки');
        }
    }


    public function clearDir($dir)
    {
        if (!is_dir($dir)) {
            return;
        }

        $files = glob($dir . '/*', GLOB_MARK);

        foreach ($files as $file) {
            $file = rtrim($file, '/');
            if (is_link($file)) {
                unlink($file);
            } else {
                $file = realpath($file);
                if (is_dir($file)) {
                    $this->clearDir($file);
                    rmdir($file);
                } else {
                    unlink($file);
                }
            }
        }
    }
}
