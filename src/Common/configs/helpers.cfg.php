<?php

use Ox3a\Common\View\HtmlView;

return [
    'view' => [
        'js'    => HtmlView\JsHelper::class,
        'css'   => HtmlView\CssHelper::class,
        'url'   => HtmlView\UrlHelper::class,
        'route' => HtmlView\RouteHelper::class,
    ],
];
