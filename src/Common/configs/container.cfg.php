<?php

return [
    'services' => [
        Zend\Router\RouteStackInterface::class          => [
            'concrete' => Zend\Router\Http\TreeRouteStack::class,
        ],
        Zend\Stdlib\RequestInterface::class             => [
            'concrete' => Ox3a\Common\Service\RequestService::class,
        ],
        Zend\Stdlib\DispatchableInterface::class        => [
            'concrete'  => Ox3a\Common\Service\DispatcherService::class,
            'arguments' => [
                Ox3a\Common\StudentApplication::class,
                Ox3a\Common\Service\RouterService::class,
                Ox3a\Common\Service\ErrorCatcherService::class,
                Ox3a\Common\Service\EventBusService::class,
            ],
        ],
        // View
        Ox3a\Common\Service\ViewServiceInterface::class => [
            'concrete'  => Ox3a\Common\Service\ViewService::class,
            'arguments' => [
                Psr\Container\ContainerInterface::class,
            ],
        ],
        // события приложения
        Ox3a\Common\Service\EventBusService::class      => [],
    ],
];
