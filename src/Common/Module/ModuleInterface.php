<?php


namespace Ox3a\Common\Module;


use Ox3a\Common\StudentApplication;

interface ModuleInterface
{

    /**
     * @param StudentApplication $app
     * @return void
     */
    public function setApp(StudentApplication $app);


    public function getName();


    public function getConfigDir();


    public function getResourceDir();


    public function bootstrap();
}
