<?php


namespace Ox3a\Common\Service;


use Zend\Http\PhpEnvironment\Request;

class RequestService extends Request implements ShareServiceInterface
{
    public function getJson($assoc = true)
    {
        return json_decode($this->getContent(), $assoc);
    }
}
