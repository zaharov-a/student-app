<?php


namespace Ox3a\Common\Service;


use Exception;
use Ox3a\Service\ConfigServiceInterface;

class ErrorCatcherService implements ShareServiceInterface
{
    /**
     * @var ConfigServiceInterface
     */
    protected $_configService;

    /**
     * @var EventBusService
     */
    protected $_eventBusService;


    /**
     * ErrorCatcherService constructor.
     * @param ConfigServiceInterface $_configService
     * @param EventBusService        $_eventBusService
     */
    public function __construct(ConfigServiceInterface $_configService, EventBusService $_eventBusService)
    {
        $this->_configService   = $_configService;
        $this->_eventBusService = $_eventBusService;
    }


    /**
     * @param Exception $e
     * @return array|null
     * @todo Добавить логгирование ошибок
     */
    public function caught(Exception $e)
    {
        $this->_eventBusService->trigger('catchError', null, [$e]);

        if (!empty($this->_configService->app['debug'])) {
            $errorData = [
                'log' => [
                    'file'  => $e->getFile(),
                    'line'  => $e->getLine(),
                    // print_r getTrace() при определенных обстоятельствах может выжрать память
                    'trace' => $e->getTraceAsString(),
                ],
            ];
        } else {
            $errorData = null;
        }

        return $errorData;
    }
}
