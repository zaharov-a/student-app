<?php

namespace Ox3a\Common\Service;


use Ox3a\Common\Controller\ControllerInterface;
use Ox3a\Common\StudentApplication;
use Ox3a\Common\View\ViewInterface;
use Psr\Container\ContainerInterface;
use RuntimeException;
use Zend\Http\Header\ContentType;
use Zend\Router\RouteMatch;
use Zend\Router\RouteStackInterface;
use Zend\Stdlib\DispatchableInterface;
use Zend\Stdlib\RequestInterface;
use Zend\Stdlib\Response;
use Zend\Stdlib\ResponseInterface;
use Exception;

/**
 * Created by PhpStorm.
 * User: aa
 * Date: 19.03.19
 * Time: 20:33
 */
class DispatcherService implements DispatchableInterface
{
    const IS_SHARE = true;

    /**
     * @var RouteStackInterface
     */
    protected $_router;

    /**
     * @var RouterService
     */
    protected $_routerService;

    /**
     * @var ContainerInterface
     */
    protected $_container;

    /**
     * @var ErrorCatcherService
     */
    protected $_errorCatcherService;

    /**
     * @var EventBusService
     */
    protected $_eventBusService;

    /**
     * @var StudentApplication
     */
    protected $_app;

    /**
     * @var RouteMatch
     */
    protected $_match;

    /**
     * @var ControllerInterface
     */
    protected $_controller;

    /**
     * @var string
     */
    protected $_actionMethod;

    /**
     * @var RequestInterface
     */
    protected $_request;

    /**
     * @var ResponseInterface
     */
    protected $_response;


    /**
     * DispatcherService constructor.
     * @param StudentApplication  $app
     * @param RouterService       $routerService
     * @param ErrorCatcherService $errorCatcherService
     * @param EventBusService     $eventBusService
     */
    public function __construct(
        StudentApplication $app,
        RouterService $routerService,
        ErrorCatcherService $errorCatcherService,
        EventBusService $eventBusService
    ) {
        $this->_app                 = $app;
        $this->_routerService       = $routerService;
        $this->_errorCatcherService = $errorCatcherService;
        $this->_eventBusService     = $eventBusService;

        $this->_container = $app->getContainer();
        $this->_router    = $routerService->getRouter();
    }


    /**
     * @param RouteMatch $match
     * @return DispatcherService
     */
    public function setMatch($match)
    {
        $this->_match = $match;
        return $this;
    }


    /**
     * @param RequestInterface $request
     * @return DispatcherService
     */
    public function setRequest($request)
    {
        $this->_request = $request;
        return $this;
    }


    /**
     * @param ResponseInterface $response
     * @return DispatcherService
     */
    public function setResponse($response)
    {
        $this->_response = $response;
        return $this;
    }


    /**
     * @param RequestInterface       $request
     * @param null|ResponseInterface $response
     * @return mixed|Response|ResponseInterface
     */
    public function dispatch(RequestInterface $request, ResponseInterface $response = null)
    {
        /** @var \Zend\Http\Response $response */
        $this->_request  = $request;
        $this->_response = $response;

        try {
            $view = $this->process();
        } catch (Exception $e) {
            $view = $this->_processError($e);
        }

        if ($view instanceof ResponseInterface) {
            return $view;
        }

        $view = $this->getViewService()->processView($view);

        $response->setStatusCode($view->getStatusCode());
        $response->getHeaders()->addHeader(new ContentType($view->getContentType()));
        $response->setContent($view->toString());

        return $response;
    }


    /**
     * @return RouteMatch
     */
    public function getMatch()
    {
        if (!$this->_match) {
            if (!($this->_match = $this->_router->match($this->_request))) {
                throw new RuntimeException(
                    sprintf('Route "%s" not found', $this->_request->getRequestUri()),
                    1401
                );
            }
        }
        return $this->_match;
    }


    /**
     * @return ViewInterface|ResponseInterface
     */
    public function process()
    {
        $this->_controller   = $this->getController();
        $this->_actionMethod = $this->getActionMethod();

        if (!$this->_controller) {
            throw new RuntimeException(
                sprintf('Controller "%s" not found', $this->getControllerClass()),
                1402
            );
        }

        if (!method_exists($this->_controller, $this->_actionMethod)) {
            throw new RuntimeException(
                sprintf('Action "%s" not found in "%s"', $this->_actionMethod, $this->getControllerClass()),
                1403
            );
        }

        return $this->run();
    }


    /**
     * @return null|ControllerInterface
     */
    public function getController()
    {
        $controllerClass = $this->getControllerClass();
        if ($this->_container->has($controllerClass)) {
            return $this->_container->get($controllerClass);
        }

        return null;
    }


    public function getControllerClass()
    {
        if (!$this->getMatch()) {
            return "";
        }

        return $this->getMatch()->getParam('_controller');
    }


    public function getActionName()
    {
        return $this->getMatch()->getParam('_actionName');
    }


    public function getActionMethod()
    {
        return $this->getMatch()->getParam('_action');
    }


    public function getModuleName()
    {
        return $this->getMatch()->getParam('_moduleName');
    }


    /**
     * @return ViewInterface|ResponseInterface
     */
    public function run()
    {
        $params      = $this->getMatch()->getParams();
        $eventResult = $this->_eventBusService->trigger('dispatcher.preRun', null, $params);

        foreach ($eventResult as $eventItem) {
            if ($eventItem instanceof ResponseInterface) {
                return $eventItem;
            }
        }

        $controller = $this->_controller;

        $controller->setRequest($this->_request);
        $controller->setResponse($this->_response);
        $controller->setParams($params);
        $view = $controller->init();

        if (!$view) {
            $view = call_user_func_array([$controller, $this->_actionMethod], $this->getActionParams());
        }

        if ($view instanceof ViewInterface || $view instanceof ResponseInterface) {
            return $view;
        }

        return $this->getViewService()->getDefaultView()->setResult($view);
    }


    public function getActionParams()
    {
        return array_diff_key(
            $this->getMatch()->getParams(),
            array_flip(
                [
                    '_module',
                    '_moduleName',
                    '_action',
                    '_actionName',
                    '_controller',
                    '_controllerDir',
                ]
            )
        );
    }


    public function getResourcesDir()
    {
        return $this->_app->getModule($this->getModuleName())->getResourceDir();
    }


    public function getControllerDir()
    {
        return $this->getMatch()->getParam('_controllerDir');
    }


    /**
     * @return ViewServiceInterface
     */
    public function getViewService()
    {
        return $this->_container->get(ViewServiceInterface::class);
    }


    /**
     * @param Exception $e
     * @return ViewInterface
     */
    protected function _processError(Exception $e)
    {
        $errorData = $this->_errorCatcherService->caught($e);

        return $this->getViewService()->getErrorView($e, $errorData);
    }

}
