<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      25.10.2020
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\Common\Service;


use Ox3a\Common\Service\Dispatcher\AclServiceInterface;
use Ox3a\Common\Service\Dispatcher\IsGrantedServiceInterface;
use Ox3a\Service\ConfigServiceInterface;
use Zend\Stdlib\ResponseInterface;

class SecurityService implements ShareServiceInterface
{
    /**
     * @var AclServiceInterface
     */
    private $_aclService;

    /**
     * @var IsGrantedServiceInterface
     */
    private $_isGrantedService;

    /**
     * @var ConfigServiceInterface
     */
    private $_configService;

    private $_checkMethods = [
        'init',
        'preDispatch',
    ];


    /**
     * SecurityService constructor.
     * @param AclServiceInterface       $_aclService
     * @param IsGrantedServiceInterface $_isGrantedService
     * @param ConfigServiceInterface    $_configService
     */
    public function __construct(
        AclServiceInterface $_aclService,
        IsGrantedServiceInterface $_isGrantedService,
        ConfigServiceInterface $_configService
    ) {
        $this->_aclService       = $_aclService;
        $this->_isGrantedService = $_isGrantedService;
        $this->_configService    = $_configService;
    }


    /**
     * @param $routeParams
     * @return ResponseInterface|null
     */
    public function check($routeParams)
    {
        $controllerClass = $routeParams['_controller'];
        $action          = $routeParams['_action'];

        foreach ($this->_checkMethods as $method) {
            if (($result = $this->_checkMethod($controllerClass, $method))) {
                return $result;
            }
        }

        return $this->_checkMethod($controllerClass, $action);
    }


    private function _checkMethod($controller, $method)
    {
        $aclCfg     = $this->_configService->get('acl');
        $grantedCfg = $this->_configService->get('granted');

        if (isset($grantedCfg[$controller][$method])) {
            $value = $grantedCfg[$controller][$method][0]['type'];

            if (($result = $this->_isGrantedService->denied($value))) {
                return $result;
            }
        }

        if (isset($aclCfg[$controller][$method])) {
            $value = $aclCfg[$controller][$method][0];

            if (($result = $this->_aclService->denied($value['path'], $value['expected']))) {
                return $result;
            }
        }
    }


}
