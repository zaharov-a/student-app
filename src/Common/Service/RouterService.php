<?php


namespace Ox3a\Common\Service;


use Ox3a\Service\ConfigServiceInterface;
use Zend\Router\RouteStackInterface;

class RouterService implements ShareServiceInterface
{

    /**
     * @var RouteStackInterface
     */
    protected $_router;


    public function __construct(ConfigServiceInterface $config, RouteStackInterface $router)
    {
        $router->setRoutes($config->routes);
        $this->_router = $router;
    }


    /**
     * @return RouteStackInterface
     */
    public function getRouter()
    {
        return $this->_router;
    }


    /**
     * @param              $routeName
     * @param array        $params
     * @param string|array $query
     * @param array        $options
     * @return mixed
     */
    public function getUrl($routeName, $params = [], $query = null, $options = [])
    {
        $options['name'] = $routeName;

        if ($query) {
            $options['query'] = $query;
        }

        return $this->_router->assemble($params, $options);
    }

}
