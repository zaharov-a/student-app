<?php


namespace Ox3a\Common\Service;


use Exception;
use Ox3a\Common\View\HtmlView;
use Ox3a\Common\View\JsonRpc20View;
use Ox3a\Common\View\ViewInterface;
use Psr\Container\ContainerInterface;
use Zend\Http\Header\Accept;
use Zend\Stdlib\DispatchableInterface;
use Zend\Stdlib\RequestInterface;

class ViewService implements ViewServiceInterface
{

    const IS_SHARE = true;

    /**
     * @var DispatcherService
     */
    protected $_dispatcher;

    /**
     * @var ContainerInterface
     */
    protected $_container;

    /**
     * @var RequestInterface
     */
    protected $_request;


    /**
     * ViewService constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->_container  = $container;
        $this->_request    = $container->get(RequestInterface::class);
        $this->_dispatcher = $container->get(DispatchableInterface::class);
    }


    public function getDefaultView()
    {
        /** @var Accept $accept */
        $accept = $this->_request->getHeader('accept');

        $jsonPriority = $accept->hasMediaType('application/json')
            ? $accept->match('application/json')->getPriority()
            : 0;
        $htmlPriority = $accept->hasMediaType('text/html')
            ? $accept->match('text/html')->getPriority()
            : 0.1;

        if ($htmlPriority > $jsonPriority) {
            /** @var HtmlView $view */
            return $this->_container->get(HtmlView::class);
        }

        return new JsonRpc20View();
    }


    public function getErrorView(Exception $exception, $extraData)
    {
        $view = $this->getDefaultView();

        $code    = $exception->getCode();
        $message = $exception->getMessage();

        if ($code >= 1400 && $code < 1500) {
            $view->setStatusCode(404);
        }

        if ($view instanceof HtmlView) {
            $view->setResult(
                [
                    'code'      => $code,
                    'message'   => $message,
                    'errorData' => $extraData,
                ]
            );
            $view->setLayout('error')
                ->disableContent();
        } else {
            $view->setError($code, $message, $extraData);
        }

        return $view;
    }


    public function processView(ViewInterface $view)
    {
        if ($view instanceof HtmlView && $view->contentIsEnable()) {
            $dispatcher = $this->_dispatcher;
            if (!$view->getTemplatesDir()) {
                $view->setTemplatesDir(
                    $dispatcher->getResourcesDir() . '/templates/' . $dispatcher->getControllerDir()
                );
            }
            if (!$view->getTemplate()) {
                $view->setTemplate($dispatcher->getActionName());
            }
        }

        return $view;
    }


}
