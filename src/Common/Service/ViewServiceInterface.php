<?php

namespace Ox3a\Common\Service;

use Exception;
use Ox3a\Common\View\ViewInterface;

interface ViewServiceInterface
{
    /**
     * @return ViewInterface
     */
    public function getDefaultView();


    /**
     * @param Exception $exception
     * @param mixed     $extraData
     * @return ViewInterface
     */
    public function getErrorView(Exception $exception, $extraData);


    /**
     * @param ViewInterface $view
     * @return ViewInterface
     */
    public function processView(ViewInterface $view);
}
