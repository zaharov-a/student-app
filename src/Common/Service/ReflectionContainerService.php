<?php


namespace Ox3a\Common\Service;


use League\Container\Exception\NotFoundException;
use League\Container\ReflectionContainer;
use ReflectionClass;
use ReflectionException;

/**
 * Сервис-контейнер с кэшированием объектов, если в классе установлена константа IS_SHARE = true
 * Class ReflectionContainerService
 * @package Ox3a\Common\Service
 */
class ReflectionContainerService extends ReflectionContainer implements ShareServiceInterface
{

    protected $_cache = [];


    /**
     * @param string $id
     * @param array  $args
     * @return object
     * @throws ReflectionException
     */
    public function get($id, array $args = [])
    {
        if (!$this->has($id)) {
            throw new NotFoundException(
                sprintf('Alias (%s) is not an existing class and therefore cannot be resolved', $id)
            );
        }

        $isShare = defined("{$id}::IS_SHARE") ? ($id::IS_SHARE) : false;

        if ($isShare === true && array_key_exists($id, $this->_cache)) {
            return $this->_cache[$id];
        }

        $reflector = new ReflectionClass($id);
        $construct = $reflector->getConstructor();

        $resolution = $construct === null
            ? new $id
            : $resolution = $reflector->newInstanceArgs($this->reflectArguments($construct, $args));

        if ($isShare === true) {
            $this->_cache[$id] = $resolution;
        }

        return $resolution;
    }
}
