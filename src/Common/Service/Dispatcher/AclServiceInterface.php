<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      11.10.2020
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\Common\Service\Dispatcher;


use Ox3a\Common\Service\ShareServiceInterface;
use Zend\Stdlib\ResponseInterface;

interface AclServiceInterface extends ShareServiceInterface
{
    /**
     * @param string     $path
     * @param string|int $expected
     * @return ResponseInterface|null
     */
    public function denied($path, $expected);
}
