<?php


namespace Ox3a\Common\Controller;


use Ox3a\Common\Service\RouterService;
use Zend\Http\PhpEnvironment\Request;
use Zend\Http\Response;
use Zend\Stdlib\RequestInterface;
use Zend\Stdlib\ResponseInterface;

abstract class AbstractController implements ControllerInterface
{
    /**
     * @var Request
     */
    protected $_request;

    /**
     * @var Response
     */
    protected $_response;

    /**
     * @var array
     */
    protected $_params;

    /**
     * @var RouterService
     */
    protected $_routerService;


    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->_request;
    }


    /**
     * @param RequestInterface $request
     * @return AbstractController
     */
    public function setRequest(RequestInterface $request)
    {
        $this->_request = $request;
        return $this;
    }


    /**
     * @return Response
     */
    public function getResponse()
    {
        return $this->_response;
    }


    /**
     * @param ResponseInterface $response
     * @return AbstractController
     */
    public function setResponse(ResponseInterface $response)
    {
        $this->_response = $response;
        return $this;
    }


    /**
     * @return array
     */
    public function getParams()
    {
        return $this->_params;
    }


    /**
     * @param array $params
     * @return AbstractController
     */
    public function setParams($params)
    {
        $this->_params = $params;
        return $this;
    }


    public function init()
    {
    }


    public function redirect($url, $code = 302)
    {
        $response = $this->_response;
        $response->getHeaders()->addHeaderLine('Location', $url);
        $response->setStatusCode($code);

        return $response;
    }


    public function goToRoute($routeName, $params = [], $query = [])
    {
        return $this->redirect($this->_routerService->getUrl($routeName, $params, $query) ?: '/');
    }

}
