<?php


namespace Ox3a\Common\Controller;


use Zend\Stdlib\RequestInterface;
use Zend\Stdlib\ResponseInterface;

interface ControllerInterface
{

    /**
     * @return RequestInterface
     */
    public function getRequest();


    /**
     * @param RequestInterface $request
     * @return ControllerInterface
     */
    public function setRequest(RequestInterface $request);


    /**
     * @return ResponseInterface
     */
    public function getResponse();


    /**
     * @param ResponseInterface $response
     * @return ControllerInterface
     */
    public function setResponse(ResponseInterface $response);


    /**
     * @return array
     */
    public function getParams();


    /**
     * @param array $params
     * @return ControllerInterface
     */
    public function setParams($params);


    /**
     * @return void|ResponseInterface
     */
    public function init();
}
